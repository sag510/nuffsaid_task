import csv
from datetime import datetime


def search_schools(search_string):

    start_time = datetime.now()

    school_data_dict = dict()
    search_count_dict = dict()
    top_3_counter = 0

    with open('school_data.csv', 'r', encoding='ISO-8859-1') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        search_string_arr = search_string.split(' ')

        # eliminating school keyword as
        # it impacts search, as school would be common
        # across school names
        for string in search_string_arr:
            if string.lower() == 'school':
                search_string_arr.remove(string)
        for row in csv_reader:
            unique_search_set = set()
            # ignore first line as it as column headers
            if line_count == 0:
                pass
            else:
                # create school dictionary,used to final display results
                school_data_dict[row[0]] = {'school_data': {'leaid':row[1], 'LEANM05': row[2],
                                                                     'school_name': row[3], 'city_name': row[4],
                                                                     'state_name': row[5]}}


                school_name_arr = split_word_by_space(row[3])
                city_name_arr = split_word_by_space(row[4])
                state_name_arr = split_word_by_space(row[5])
                school_name_match_count = 0
                city_name_match_count = 0
                state_name_match_count = 0
                total_name_match_count = 0

                for each_search_word in search_string_arr:

                    # matching each search word across each word of school name
                    school_name_match_count, unique_search_set = search_word_across_entity_name(each_search_word,
                                                                                              school_name_arr,
                                                                                              school_name_match_count,
                                                                                              unique_search_set)
                    # matching each search word across each word of city name
                    city_name_match_count, unique_search_set = search_word_across_entity_name(each_search_word,
                                                                                               city_name_arr,
                                                                                               city_name_match_count,
                                                                                               unique_search_set)
                    # redundant as we diont ahev state name in data set,
                    # but added as user might search for AL for alabama

                    # matching each search word across each word of state name
                    state_name_match_count, unique_search_set = search_word_across_entity_name(each_search_word,
                                                                                               state_name_arr,
                                                                                               state_name_match_count,
                                                                                               unique_search_set)

                total_name_match_count = school_name_match_count + city_name_match_count + state_name_match_count
                unique_search_count = unique_search_set.__len__()
                search_count_dict[row[0]]= {'state_name_match_count': state_name_match_count,
                                                       'city_name_match_count': city_name_match_count,
                                                       'school_name_match_count':  school_name_match_count,
                                                       'total_name_match_count': total_name_match_count,
                                                       'unique_search_count':unique_search_count
                                                      }

            line_count += 1

            # Adding optimized logic,
            # if we get all search string character matched, then incrememnt a counter
            if unique_search_set.__len__() == search_string_arr.__len__():
                top_3_counter +=1
            # if we such all matching top 3 entries then we should terminate the search
            if top_3_counter == 3:
                break
        start_time = datetime.now()
        search_new_dict = dict()

        # Remove those entries which didnt match any search string
        for keys, values in search_count_dict.items():
            if values['total_name_match_count'] > 0:
                search_new_dict[keys] = values

        # sorting the final data set depending on those which has maximum search string count matcch
        # then school_name_match
        # then city_name match
        # then state_name match
        if search_new_dict.__len__() > 0:
            sorted_result = sorted(search_new_dict.items(), key=lambda x: (x[1]['unique_search_count'], x[1]['school_name_match_count'], x[1]['city_name_match_count'],x[1]['state_name_match_count']), reverse=True)
            count = 0

            print('Results for "' + search_string + '" search took: ' + str((datetime.now().microsecond - start_time.microsecond)/1000) + 'ms')

            for data in sorted_result:
                if count == 3:
                    break
                else:
                    print(str((count + 1)) + '.' + school_data_dict.get(data[0]).get('school_data').get('school_name'))
                    print(school_data_dict.get(data[0]).get('school_data').get('city_name') + ', ' + school_data_dict.get(data[0]).get('school_data').get('state_name'))
                count +=1
        else:

            print('No Results found for "' + search_string + '". Search took: ' + str(
                (datetime.now().microsecond - start_time.microsecond) / 1000) + 'ms')


def split_word_by_space(word):
    split_word_arr = word.split(' ')
    return split_word_arr


def search_word_across_entity_name(word, entity, entity_count, unique_search_set):
    for each_word in entity:
        if word.lower() == each_word.lower():
            entity_count = entity_count + 1
            unique_search_set.add(word)

    return entity_count, unique_search_set