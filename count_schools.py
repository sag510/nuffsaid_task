import csv
from datetime import datetime


def print_counts():

    school_data_dict = dict()
    state_wise_school_dict = dict()
    metro_centric_school_dict = dict()
    city_wise_school_dict = dict()
    unique_city_wise_school_dict = dict()

    with open('school_data.csv', 'r', encoding='ISO-8859-1') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                school_data_dict[row[0]]= {'school_data': {'school_name': row[3]}}
                # creating state wise dictionary for school count of each state
                create_entity_wise_dictionary_with_count(state_wise_school_dict, row[5])

                # creating metro-centric-locale wise dictionary for school count of each state
                create_entity_wise_dictionary_with_count(metro_centric_school_dict, row[8])

                # creating city wise dictionary for school count of each state
                create_entity_wise_dictionary_with_count(city_wise_school_dict, row[4])

                if row[2] != '':
                    # creating unique city wise dictionary for school count of each state
                    create_entity_wise_dictionary_with_count(unique_city_wise_school_dict, row[2])

            line_count += 1
        start_time = datetime.now()
        print('Total Schools: ' + str(school_data_dict.__len__()))
        print('Schools by State:')
        for state, count in state_wise_school_dict.items():
            print(state + ': ' + str(count))
        print('...')
        print('Schools by Metro - centric locale:')
        for metro, count in metro_centric_school_dict.items():
            print(str(metro) + ': ' + str(count))
        print('...')
        sorted_list = sorted(city_wise_school_dict.items(), key=lambda x: x[1], reverse=True)
        city_with_schools = sorted_list[0]
        print('City with most schools: ' + city_with_schools[0] + ' (' + str(city_with_schools[1]) +  ' schools)')
        print('Unique cities with at least one school: ' + str(unique_city_wise_school_dict.__len__()))
        print('Total milliseconds taken ' + str((datetime.now().microsecond - start_time.microsecond)/1000))


def create_entity_wise_dictionary_with_count(entity, entity_data):
    if entity.__contains__(entity_data):
        enity_wise_count = entity.get(entity_data) + 1
        entity[entity_data] = enity_wise_count
    else:
        entity[entity_data]=1
